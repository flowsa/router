<?php
/**
 * router plugin for Craft CMS 3.x
 *
 * Web routing
 *
 * @link      https://www.flowsa.com
 * @copyright Copyright (c) 2019 Flow communications
 */

/**
 * router en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('router', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Flow communications
 * @package   Router
 * @since     1.0.0
 */
return [
    'router plugin loaded' => 'router plugin loaded',
];
